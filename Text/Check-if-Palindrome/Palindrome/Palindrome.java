package Palindrome;

import java.util.Scanner;
import java.io.IOException;

public class Palindrome {
    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter word to check if palindrome");
        final String x = input.next();
        isPalindrome(x);
        System.out.println(isPalindrome(x));
        input.close();
    }

    protected static boolean isPalindrome(String s) {
        for (int i = 0; i < s.length() / 2; i++) {
            if (Character.toLowerCase(s.charAt(i)) !=
                    Character.toLowerCase(s.charAt(s.length() - 1 - i))) {
                return false;
            }
        }
        return true;
    }
}
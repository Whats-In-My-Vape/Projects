package Fibonacci;

import java.util.Scanner;

public class Fibonacci {
    public static void main(String[] args){
        double num1 = 1;
        double num2 = 1;
        double nextNum;
        double stop;

        Scanner in = new Scanner(System.in);

        System.out.println("Enter a number (bigger numbers take longer)");
        stop = in.nextDouble();

        System.out.println("Here is the Fibonacci Sequence up to " + stop);
        System.out.print(num1);

        while (num2 < stop) {
            nextNum = num1 + num2;
            num1 = num2;
            num2 = nextNum;
            System.out.print(", " + num1);
        }
        in.close();
    }
}